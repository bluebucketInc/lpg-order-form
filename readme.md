# LPG Form

## Folder structure

| App | Location |
| ------ | ------ |
| Server | /server |
| Front-end | /app |
| Config | /config |
| eDM Templates | /edm |

## Technology Stalk

* Express
* Mongodb (mongoose)
* Mailgun-js
* React with Hooks
* Sentry
* PM2