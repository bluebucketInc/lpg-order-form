// import plugins
import express from "express";
import { validationResult } from "express-validator";

// utils
import { submitForm, submitLuckyDrawForm } from "../db";
import { validateToken } from "./token";
import { reqValidator, getDist, luckyDrawValidator } from "../utils";
import { IS_DEV } from "../utils/config";
import { sendOrderConfirmation, sendDistributerReceipt } from "../email";

// router
export const formRoutes = express.Router();

// generator endpoint
formRoutes.post("/form_submit", reqValidator(), (req, res) => {
	// check validation
	const errors = validationResult(req);

	// if error
	if (!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() });
	}
	
	// validate token
	validateToken(req.body.token)
		.then(() => submitForm(req.body))
		.then(id =>
			sendDistributerReceipt({
				...req.body,
				order_id: id,
				to: IS_DEV ? "bluebucket.dev@gmail.com" : `${getDist(req.body.distributor).email}, leah.layhiong.ng@exxonmobil.com, jestyn.lim@exxonmobil.com`,
			}).then(() => {
				// send order confirmation
				sendOrderConfirmation({
					order_id: id,
					to: req.body.email,
					dist: getDist(req.body.distributor)
				});
				// send response
				res.status(201).send({
					order_id: id,
					success: true
				});
			})
		)
		.catch(err => {
			console.log(err);
			res.status(400).send({
				success: false,
				message: err,
			});
		});
});

// generator endpoint
formRoutes.post("/luckydraw_submit", luckyDrawValidator(), (req, res) => {
	// check validation
	const errors = validationResult(req);
	// if error
	if (!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() });
	}
	// validate token
	validateToken(req.body.token)
		.then(() => submitLuckyDrawForm(req.body))
		.then(id =>
			// send response
			res.status(201).send({
				order_id: id,
				success: true
			})
		)
		.catch(err => {
			console.log(err);
			res.status(400).send({
				success: false,
				message: err,
			});
		});
});