
// import plugins
import ua from 'universal-analytics'
import express from "express";
import { join } from "path";

// components
import { GA_ID } from '../utils/config';
import { checkOrderID } from '../db/';

// items
export const staticRoutes = express.Router();
const visitor = ua(GA_ID);

// download pdf
staticRoutes.get('/get_recipe/:orderId', (req, res) => {
	// see if valid order
	checkOrderID(req.params.orderId)
		.then(() => {
			// fire GA
			visitor.event('Order', 'PDF-Download', req.params.orderId).send();
			// trigger download
			res.download(join(__dirname, '..', "static", "recipe-booklet.pdf"), `essolpgcookit-recipes_${req.params.orderId}.pdf`);
		})
		.catch(() => res.status(500).send({error: 'Not authorised!'}))	
});