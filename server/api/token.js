
// import plugins
import express from "express";
import TokenVal from "token-validator";

// settings
import {
	SESSION_SECRET,
	SESSION_DURATION,
	SESSION_LENGTH
} from "../utils/config";

// routes
export const tokenRoutes = express.Router();

// token generator
const tokenGen = new TokenVal(SESSION_SECRET, SESSION_DURATION, SESSION_LENGTH);

// generate token
tokenRoutes.get("/get_token", (req, res) => {
	res.status(200).send({
		_token: tokenGen.generate(Date.now(), SESSION_SECRET)
	});
});

// validate token
export const validateToken = token =>
	new Promise((resolve, reject) => {
		if (!token) {
			reject('No token provided! Please try again');
		} else if(tokenGen.verify(Date.now(), SESSION_SECRET, token)){
			resolve();
		} else {
			reject('Your token expired! Please try again');
		}
	});