
// import plugins
import mailgun from "mailgun-js";
import { getDist } from "../utils";

// settings
import {
	MG_API_KEY,
	MG_DOMAIN,
	MG_TEMPLATE_CONFIRM,
	MG_TEMPLATE_DIST,
	MG_FROM_ADDRESS,
	PDF_ENDPOINT,
	DOMAIN
} from "../utils/config";

// Mailgin instance
const mGun = mailgun({ apiKey: MG_API_KEY, domain: MG_DOMAIN });

// send order confirmation
export const sendOrderConfirmation = data => 
	new Promise((resolve, reject) =>
		// trigger mGun
		mGun.messages().send({
			from: MG_FROM_ADDRESS,
			to: data.to,
			subject: "Esso LPG Cylinder Order Received!",
			template: MG_TEMPLATE_CONFIRM,
			"h:X-Mailgun-Variables": JSON.stringify({
				order_id: data.order_id,
				dist_name: data.dist.name,
				dist_phone: data.dist.phone,
				download_link: DOMAIN + PDF_ENDPOINT + data.order_id,
			})
		}, (error, body) => error ? reject(error) : resolve(body))
	);


// send distributer email
export const sendDistributerReceipt = data => 
	new Promise((resolve, reject) =>
		// trigger mGun
		mGun.messages().send({
			from: MG_FROM_ADDRESS,
			to: data.to,
			subject: "You’ve Received a New Esso LPG Cylinder Order!",
			template: MG_TEMPLATE_DIST,
			"h:X-Mailgun-Variables": JSON.stringify({
				...data,
				distributor: getDist(data.distributor).name,
			})
		}, (error, body) => error ? reject(error) : resolve(body))
	);