
// imports
import { join } from 'path';

// get enviornment congif
require('dotenv-flow').config({path: join(__dirname, '..', '..', 'config')})

// export config
export const

	// dev
	IS_DEV = process.env.NODE_ENV === "production" ? false : true,
	
	// databse
	DB_URL = process.env.DB_URL,
	DB_USER = process.env.DB_USER,
	DB_PASSWORD = process.env.DB_PASSWORD,

	// api
	API_PORT = process.env.API_PORT,
	PDF_ENDPOINT = process.env.DOWNLOAD_PDF,
	DOMAIN = process.env.DOMAIN,

	// session
	SESSION_SECRET = process.env.SESSION_SECRET,
	SESSION_DURATION = 60 * 1000 * parseInt(process.env.SESSION_DURATION),
	SESSION_LENGTH = parseInt(process.env.SESSION_LENGTH),

	// order
	ORDER_ID_LN = parseInt(process.env.ORDER_ID_LN),

	// analytics
	GA_ID = process.env.GA_ID,

	// email
	MG_DOMAIN = process.env.MG_DOMAIN,
	MG_API_KEY = process.env.MG_API_KEY,
	MG_TEMPLATE_CONFIRM = process.env.MG_TEMPLATE_CONFIRM,
	MG_TEMPLATE_DIST = process.env.MG_TEMPLATE_DIST,
	MG_FROM_ADDRESS = process.env.MG_FROM_ADDRESS;