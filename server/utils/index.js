// imports
import randomize from "randomatic";
import { ORDER_ID_LN } from "./config";
import { body } from "express-validator";
import distributors from "./distributors";

// generate order id
export const genOrderID = (id) =>
  distributors[id].prefix + randomize("A0", ORDER_ID_LN);

// get distributer email
export const getDist = (id) => {
  return distributors[id];
};

// express validator
export const reqValidator = () => [
  // sanity
  body("first_name").not().isEmpty().trim().escape(),
  body("last_name").trim().escape(),
  body("unit").not().isEmpty().trim().escape(),
  body("blk").not().isEmpty().trim().escape(),
  body("street").not().isEmpty().trim().escape(),
  body("distributor").not().isEmpty().trim().escape(),
  body("timeslot").not().isEmpty().trim().escape(),

  // specific
  body("email").not().isEmpty().trim().escape().isEmail().normalizeEmail(),
  body("phone").not().isEmpty().trim().escape().isNumeric(),
  body(".postcode").not().isEmpty().trim().escape().isNumeric(),
  body("quantity")
    .not()
    .isEmpty()
    .trim()
    .escape()
    .isNumeric()
    .isLength({ max: 1 }),
  body("terms_consent").not().isEmpty().trim().escape().isBoolean(),
  body("mktg_consent").not().isEmpty().trim().escape().isBoolean(),
];

// express validator
export const luckyDrawValidator = () => [
  // sanity
  body("first_name").not().isEmpty().trim().escape(),
  body("last_name").trim().escape(),
  body("distributor").not().isEmpty().trim().escape(),
  body("modeOfContact").not().isEmpty().trim().escape(),

  // specific
  body("email").not().isEmpty().trim().escape().isEmail().normalizeEmail(),
  body("phone").not().isEmpty().trim().escape().isNumeric(),
  body("receipt_no").not().isEmpty().trim().escape(),
  body("terms_consent").not().isEmpty().trim().escape().isBoolean(),
  body("mktg_consent").not().isEmpty().trim().escape().isBoolean(),
];
