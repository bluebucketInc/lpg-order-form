// distibuter details
export default {
  a: {
    name: "Tan Bros Gas Supply",
    email: "tanbrosgassupply@gmail.com",
    // email: "bluebucket.dev@gmail.com",
    prefix: "TB-",
    phone: "6455 1169",
  },
  b: {
    name: "Tan Soon Huah Gas Supply",
    email: "sales@tshgas.com.sg",
    // email: "bluebucket.dev@gmail.com",
    prefix: "TSH-",
    phone: "6289 5554",
  },
  c: {
    name: "Mega Gas Enterprise Pte Ltd",
    email: "hello@megagas.com.sg",
    // email: "bluebucket.dev@gmail.com",
    prefix: "MGE-",
    phone: "6899 3388",
  },
  // c: {
  // 	name: "No preferred distributor",
  // 	email: "sales@tshgas.com.sg",
  // 	prefix: "ELPG-",
  // 	phone: "1800-266-2828",
  // },
};

// // distibuter details
// export default {
// 	a: {
// 		name: "Tan Bros Gas Supply",
// 		email: "leah.layhiong.ng@exxonmobil.com",
// 		prefix: "TB-",
// 		phone: "6455 1169",
// 	},
// 	b: {
// 		name: "Tan Soon Huah Gas Supply",
// 		email: "jestyn.lim@exxonmobil.com",
// 		prefix: "TSH-",
// 		phone: "6289 5554",
// 	},
// 	c: {
// 		name: "No preferred distributor",
// 		email: "sales@tshgas.com.sg",
// 		prefix: "ELPG-",
// 		phone: "1800-266-2828",
// 	},
// };
