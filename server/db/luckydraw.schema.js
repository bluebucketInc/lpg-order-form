// lib imports
import mongoose from "mongoose";

// Form Data Schema
let luckydraw_form = new mongoose.Schema({
  created_at: Date,
  first_name: String,
  last_name: String,
  email: String,
  phone: String,
  receipt_no: String,
  distributor: String,
  modeOfContact: String,
  mktg_consent: Boolean,
  terms_consent: Boolean,
  created_at_formatted: String,
});

// export
module.exports = mongoose.model(
  "luckydraw_form",
  luckydraw_form,
  "luckydraw_form"
);
