
// lib imports
import mongoose from "mongoose";

// Form Data Schema
let lpg_form = new mongoose.Schema({
	order_id: String,
	order_date: String,
	first_name: String,
	last_name: String,
	email: String,
	phone: String,
	blk: String,
	unit: String,
	street: String,
	postcode: String,
	distributor: String,
	quantity: Number,
	delivery_date: String,
	timeslot: String,
	mktg_consent: Boolean,
	terms_consent: Boolean,
	existing_customer: String,
	smiles_id: String,
	created_at: Date,
	modeOfContact: String,
});

// export
module.exports = mongoose.model("lpg_form", lpg_form, "lpg_form");