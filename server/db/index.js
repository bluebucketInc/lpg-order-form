
// import plugins
import moment from "moment";
import mongoose from "mongoose";
import { genOrderID, getDist } from '../utils';

// settings
import { DB_URL, DB_USER, DB_PASSWORD } from "../utils/config";

// schema
import lpg_form from '../db/db.schema';
import luckydraw_form from '../db/luckydraw.schema';

// connect to db
mongoose.connect( DB_URL, {
		useNewUrlParser: true,
		user: DB_USER,
		pass: DB_PASSWORD,
	})
	.then(db => console.log("connection successful"))
	.catch(e => console.log("connection error", e));



// Add form to DB
export const submitForm = payload =>
	new Promise((resolve, reject) => {
		const order_id = genOrderID(payload.distributor);
		// form instance
		let item = new lpg_form({
			created_at: moment(Date.now()),
			order_date: moment(Date.now()).format("DD/MM/YY"),
			order_id: order_id,
			...payload,
			distributor: getDist(payload.distributor).name,
		});
		// send
		item.save()
			.then(() => resolve(order_id))
			.catch(err => reject(err));
	});

// lucky draw form
export const submitLuckyDrawForm = payload =>
	new Promise((resolve, reject) => {
		// form instance
		let item = new luckydraw_form({
			created_at: moment(Date.now()),
			created_at_formatted: moment(Date.now()).format("DD/MM/YY"),
			...payload,
			distributor: getDist(payload.distributor).name,
		});
		// send
		item.save()
			.then(() => resolve())
			.catch(err => reject(err));
	});

// check if order id exists
export const checkOrderID = order_id =>
	new Promise((resolve, reject) =>
		lpg_form.find({order_id})
			.then(r => r.length > 0 ? resolve() : reject())
	);