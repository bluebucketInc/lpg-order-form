// import plugins
import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";

// settings
import { API_PORT } from "./utils/config";

// API routes
import { formRoutes } from "./api/form";
import { tokenRoutes } from "./api/token";
import { staticRoutes } from "./api/static";

// init server
const server = express();

// add middleware
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// security
server.use(helmet());

// end points
server.use("/", staticRoutes);
server.use("/api", formRoutes);
server.use("/auth", tokenRoutes);

// broadcast
server.listen(API_PORT, () => console.log(`server running on port ${API_PORT}`));