# Server

## Installation

Install [PM2](http://pm2.keymetrics.io/) globally in server
> npm install pm2 -g

Install node modules in *server* folder
> npm install

Run application with PM2 in *server* folder
> pm2 start server.config.js

Use below commands to monitor the application
> pm2 list
// list of running applications, application state should be 'running' in green 

> pm2 logs
// view application logs, application log should end with 'connection successful'

All other commands [here](https://devhints.io/pm2).

## Production Config

Update below config variables to reflect production environment

> DOMAIN
> MG_DOMAIN
> MG_API_KEY

## Restart application after update

Find PM2 app ID
> pm2 list

Restart application
> pm2 restart &lt;app id&gt;

## Monitor Application Health

Application is monitored using [PM2 online console](https://app.pm2.io/bucket/5d353059dc9b7c7d1668bd04/backend/overview/servers).

Access details are in google drive - *Creds*.

## Auto-start application on startup

Follow [these instructions](http://pm2.keymetrics.io/docs/usage/startup/) to auto start application

After updating the app. Follow steps under 'Updating startup script' to update the script.