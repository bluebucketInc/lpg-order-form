
// imports
import axios from "axios";
import moment from "moment";
import { captureEvent } from '@sentry/browser';
import ReactGA from 'react-ga';

// components
import { ENDPOINTS } from "../utils";
import { selectValues } from "../utils/form_settings";

/*

	ACTION TYPE: FORM EDITS

*/

// handle date change in form
export const handleFormDateChange = date => dispatch =>
	dispatch({
		type: "form-change",
		payload: { delivery_date: date },
	});

// handle form content change
export const handleFormChange = (e, name, isCheckbox) => dispatch => {
	
	// force distributor
	// if (name === "existing_customer" && e.target.value === "b") {
	// 	dispatch({
	// 		type: "form-change",
	// 		payload: { "distributor": "c" },
	// 	});
	// }

	// form change
	dispatch({
		type: "form-change",
		payload: { [name]: isCheckbox ? e.target.checked : e.target.value },
	});
}

// handle form content change
export const handleLuckyDrawFormChange = (e, name, isCheckbox) => dispatch => {
	
	// force distributor
	// if (name === "existing_customer" && e.target.value === "b") {
	// 	dispatch({
	// 		type: "form-change",
	// 		payload: { "distributor": "c" },
	// 	});
	// }

	// form change
	dispatch({
		type: "luckydraw-form-change",
		payload: { [name]: isCheckbox ? e.target.checked : e.target.value },
	});
}

// on form edit
export const handleEditForm = () => dispatch =>
	dispatch({
		type: "order-confirmation",
		payload: false,
	});


/*

	ACTION TYPE: FORM SUBMISSION EVENTS

*/


// submttion error
export const resetSubmissionError = bool => dispatch =>
	dispatch({
		type: "show-network-error",
		payload: bool,
	});

// reset all values
export const resetValues = () => dispatch => {

	// reset values
	dispatch({
		type: "reset-values"
	});

	// get new token
	dispatch(getAccessToken());
}


/*

	ACTION TYPE: FORM SUBMISSION

*/


// handle form submit
export const handleFormSubmit = data => dispatch => {

	// analytics
	ReactGA.event({
		category: 'Order',
		action: 'Submitted',
	});

	// set processing
	dispatch({ type: "order-submission", payload: true });

	// submit form
	axios
		.post(ENDPOINTS.form_submit, {
			...data,
			timeslot: selectValues.timeslot[data.timeslot],
			delivery_date: moment(data.delivery_date).format("DD/MM/YY"),
		})
		.then(res =>
			dispatch({
				type: "submission-success",
				payload: res.data.order_id,
			})
		)
		.catch(err => {
	
			// see error
			console.log(err);

			// sentry
			captureEvent({
				message: 'Form submission error',
  				stacktrace: err
			});

			// show error
			dispatch({
				type: "show-network-error",
				payload: true,
			})

			// reset submittion
			dispatch({
				type: "submission-failed",
				payload: err,
			});
		});
};

// handle form submit
export const handleLuckyDrawFormSubmit = data => dispatch => {

	// analytics
	ReactGA.event({
		category: 'LuckydrawForm',
		action: 'Submitted',
	});

	// set processing
	dispatch({ type: "order-submission", payload: true });
	// submit form
	axios
		.post(ENDPOINTS.luckydraw_submit, data)
		.then(res =>
			dispatch({
				type: "luckydraw-submission-success",
			})
		)
		.catch(err => {
	
			// see error
			console.log(err);

			// sentry
			captureEvent({
				message: 'Form submission error',
  				stacktrace: err
			});

			// show error
			dispatch({
				type: "show-network-error",
				payload: true,
			})

			// reset submittion
			dispatch({
				type: "submission-failed",
				payload: err,
			});
		});
};

/*

	ACTION TYPE: ACCESS TOKEN

*/


// get token on loan
export const getAccessToken = () => dispatch => {

	// get access token
	axios
		.get(ENDPOINTS.token)
		.then(r =>
			dispatch({
				type: "set-access-token",
				payload: r.data._token
			})
		)
		.catch(err => {
			// show error
			dispatch({
				type: "show-network-error",
				payload: true,
			});
			// sentry
			captureEvent({
				message: 'Access token error',
  				stacktrace: err
			});
		})
};
