
// imports
import { getminOrderingDate } from "../utils";

// initial state
export const initialState = {
	app: {
		isProcessing: false,
		isConfirming: false,
		formError: false,
		isNetworkError: false,
	},
	order: {
		isSuccess: false,
		id: "",
	},
	values: {
		token: "",
		first_name: '',
		last_name: '',
		email: '',
		phone: '',
		blk: '',
		unit: '',
		street: '',
		postcode: '',
		distributor: '',
		quantity: '',
		delivery_date: getminOrderingDate(),
		timeslot: '',
		smiles_id: '',
		existing_customer: '',
		modeOfContact: '',
		terms_consent: true,
		mktg_consent: false,
	},
	luckydraw: {
		token: "",
		first_name: "",
		last_name: "",
		email: "",
		phone: "",
		receipt_no: "",
		distributor: "",
		modeOfContact: "",
		terms_consent: true,
		mktg_consent: false,
	}
};

// reducer
export const reducer = (state, action) => {
	switch (action.type) {
		case "set-access-token":
			return {
				...state,
				values: {
					...state.values,
					token: action.payload,
				},
				luckydraw: {
					...state.luckydraw,
					token: action.payload,
				}
			}
		case "form-change":
			return {
				...state,
				values: {
					...state.values,
					...action.payload,
				}
			};
		case "luckydraw-form-change":
			return {
				...state,
				luckydraw: {
					...state.luckydraw,
					...action.payload,
				}
			};
		case "form-error":
			return {
				...state,
				app: {
					...state.app,
					formError: action.payload,
				}
			}
		case "order-confirmation":
			return {
				...state,
				app: {
					...state.app,
					isConfirming: action.payload,
				}
			}
		case "order-submission":
			return {
				...state,
				app: {
					...state.app,
					isProcessing: true,
				}
			}
		case "submission-success":
			return {
				...state,
				app: {
					...state.app,
					isConfirming: false,
					isProcessing: false,
				},
				order: {
					...state.order,
					isSuccess: true,
					id: action.payload,
				}
			}
		case "luckydraw-submission-success":
			return {
				...state,
				app: {
					...state.app,
					isConfirming: false,
					isProcessing: false,
				},
				order: {
					...state.order,
					isSuccess: true,
				}
			}
		case "submission-failed":
			return {
				...state,
				app: {
					...state.app,
					isConfirming: true,
					isProcessing: false,
				}
			}
		case "show-network-error":
			return {
				...state,
				app: {
					...state.app,
					isNetworkError: action.payload,
				}
			}
		case "reset-values":
			return {
				...initialState,
			}
		default:
			throw new Error();
	}
};