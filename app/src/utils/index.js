// imports
import moment from "moment";
import { join } from "path";

// env variables
require("dotenv-flow").config({
	path: join(__dirname, "..", "..", "..", "config"),
});

// settings
export const isDEV = process.env.NODE_ENV === "development" ? true : false,
	API_URL = process.env.REACT_APP_DOMAIN + process.env.REACT_APP_API_PORT,
	ENDPOINTS = {
		luckydraw_submit: process.env.REACT_APP_LUCKYDRAW_FORM_SUBMIT,
		form_submit: process.env.REACT_APP_FORM_SUBMIT,
		token: process.env.REACT_APP_TOKEN,
		pdf: process.env.REACT_APP_DOWNLOAD_PDF,
	},
	MAX_ORDER_TIME = process.env.REACT_APP_MAX_ORDER_HR, //16
	GA_ID = process.env.REACT_APP_GA_ID;

// get minimum date logic
export const getminOrderingDate = () => {
	// if order time is above 6pm
	if (moment().format("HH") >= 16) {
		// add two day
		return moment(new Date()).add(1, "days").toDate();
	} else {
		// if order time is above cut off time
		return moment(new Date()).toDate();
		// return moment(new Date()).add(1, "days").toDate();
	}
};