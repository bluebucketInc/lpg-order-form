// imports
import React from "react";
import moment from "moment";

// form settings
export const getFormSettings = classes => ({
	formControl: {
		variant: "filled",
		className: classes.formControl
	},
	textInput: {
		variant: "filled",
		className: classes.textinput
	},
	select: {
		select: true,
		variant: "filled"
	},
	datePicker: {
		format: "dd / MM / yyyy",
		disableToolbar: true,
		required: true,
		disablePast: true,
		margin: "normal",
		id: "delivery_date",
		label: "Preferred Delivery Date"
	},
	gridSpacing: 3,
	gridItem: {
		sm: 6,
		xs: 12,
		item: true,
		className: classes.gridItem
	}
});

// input items
export const inputItems = [
	{
		label: "First Name",
		id: "first_name",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Last Name",
		id: "last_name",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Email",
		id: "email",
		validators: ["required", "isEmail"],
		errorMessages: ["This field is required", "Email is not valid"]
	},
	{
		label: "Contact Number",
		id: "phone",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Block/Building Name",
		id: "blk",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Unit Number",
		id: "unit",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Street Name",
		id: "street",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Postal Code",
		id: "postcode",
		validators: ["required"],
		errorMessages: ["This field is required"]
	}
];

export const luckydrawItems = [
	{
		label: "First Name",
		id: "first_name",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Last Name",
		id: "last_name",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Email",
		id: "email",
		validators: ["required", "isEmail"],
		errorMessages: ["This field is required", "Email is not valid"]
	},
	{
		label: "Contact Number",
		id: "phone",
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Receipt Serial Number",
		id: "receipt_no",
		// validators: ["isNumber", "minStringLength:7", "maxStringLength:7","required"],
		// errorMessages: ["Please re-enter your 7-digit receipt serial number."]
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
];

// select items
export const selectItems = [
	{
		label: "Are you an existing Esso LPG customer?",
		id: "existing_customer",
		options: [
			{
				value: 'a',
				label: "Yes"
			},
			{
				value: 'b',
				label: "No"
			}
		],
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Distributor",
		id: "distributor",
		options: [
			// {
			// 	value: "c",
			// 	label: "No preferred distributor",
			// 	distributor: false,
			// },
			{
				value: "a",
				label: "Tan Bros Gas Supply",
				distributor: true,
			},
			{
				value: "b",
				label: "Tan Soon Huah Gas Supply",
				distributor: true,
			},
			{
				value: "c",
				label: "Mega Gas Enterprise Pte Ltd",
				distributor: true,
			},
		],
		validators: ["required"],
		errorMessages: ["This field is required"]
	},
	{
		label: "Quantity",
		id: "quantity",
		options: [
			{
				value: 1,
				label: 1,
			},{
				value: 2,
				label: 2,
			}
		],
		validators: ["required"],
		errorMessages: ["This field is required"]
	}
];

export const luckydrawSelectItems = [
	{
		label: "Distributor Purchased From",
		id: "distributor",
		options: [
			{
				value: "a",
				label: "Tan Bros Gas Supply",
				distributor: true,
			},
			{
				value: "b",
				label: "Tan Soon Huah Gas Supply",
				distributor: true,
			},
			{
				value: "c",
				label: "Mega Gas Enterprise Pte Ltd",
				distributor: true,
			},
		],
		validators: ["required"],
		errorMessages: ["This field is required"]
	}
];

// checkbox items
export const checkboxItems = [
	
	{
		label: (
			<p>
				I agree to the{" "}
				<a
					href="/pdfs/esso-cook-it-tnc.pdf"
					target="_blank"
					rel="noopener noreferrer"
				>
					<strong>terms and conditions</strong>
				</a>
			</p>
		),
		id: "terms_consent"
	}
];

// timeslot for today
export const todayTimeSlot = [
	{
		label: "08 AM - 10 AM",
		value: "a",
		valid: moment().format("HH") < 6,
	},
	{
		label: "10 AM - 12 PM",
		value: "b",
		valid: moment().format("HH") < 8,
	},
	{
		label: "12 PM - 02 PM",
		value: "c",
		valid: moment().format("HH") < 10,
	},
	{
		label: "02 PM - 04 PM",
		value: "d",
		valid: moment().format("HH") < 12,
	},
	{
		label: "04 PM - 06 PM",
		value: "e",
		valid: moment().format("HH") < 14,
	},
	{
		label: "06 PM - 08 PM",
		value: "f",
		valid: moment().format("HH") < 16,
	}
];

// timeslot for everyday
export const everydayTimeSlot = [
	{
		label: "08 AM - 10 AM",
		value: "a",
		valid: true
	},
	{
		label: "10 AM - 12 PM",
		value: "b",
		valid: true
	},
	{
		label: "12 PM - 02 PM",
		value: "c",
		valid: true
	},
	{
		label: "02 PM - 04 PM",
		value: "d",
		valid: true
	},
	{
		label: "04 PM - 06 PM",
		value: "e",
		valid: true
	},
	{
		label: "06 PM - 08 PM",
		value: "f",
		valid: true
	}
];


// form helper
export const selectValues = {
	distributor: {
		a: {
			name: "Tan Bros Gas Supply",
			email: "tanbrosgassupply@gmail.com",
			prefix: "TB-",
			phone: "6455 1169",
		},
		b: {
			name: "Tan Soon Huah Gas Supply",
			email: "sales@tshgas.com.sg",
			prefix: "TSH-",
			phone: "6289 5554",
		},
		c: {
			name: "Mega Gas Enterprise Pte Ltd",
			email: "hello@megagas.com.sg",
			prefix: "MGE-",
			phone: "6899 3388",
		},
	},
	timeslot: {
		a: "08 AM - 10 AM",
		b: "10 AM - 12 PM",
		c: "12 PM - 02 PM",
		d: "02 PM - 04 PM",
		e: "04 PM - 06 PM",
		f: "06 PM - 08 PM",
	},
}