// lib imports
import React, { useEffect } from "react";
import useThunkReducer from "react-hook-thunk-reducer";
import CookieConsent from "react-cookie-consent";
import ReactGA from "react-ga";
// material
import { Container } from "@material-ui/core";
// utils
import { generalStyles } from "./styles/theme";
// state & events
import { initialState, reducer } from "./state/reducer";
import {
  handleEditForm,
  handleLuckyDrawFormSubmit,
  getAccessToken,
  resetValues,
  resetSubmissionError,
} from "./state/actions";
// components
import Form from "./components/_luckydraw_form";
import ConfirmPopup from "./components/_luckydraw_poupup_confirmation";
import { LuckyDrawThankyouPopup } from "./components/_poupup_thankyou";
import ErrorSnack from "./components/_errorSnack";
import Header from "./components/_header";
import { LuckyDrawMasthead } from "./components/_masthead";
import Footer from "./components/_footer";
import { GA_ID } from "./utils";

// lucky draw form
export const LuckyDrawForm = () => {
  // hooks
  const classes = generalStyles();
  const [state, dispatch] = useThunkReducer(reducer, initialState);
  // get access token on load
  useEffect(() => dispatch(getAccessToken()), []);

  // render
  return (
    <Container maxWidth="lg" className="outerWrapper">
      {/* Header */}
      <Header luckdrawForm />
      {/* Masthead */}
      <LuckyDrawMasthead luckdrawForm />
      {/* form */}
      <Container maxWidth="md" className={classes.wrapper}>
        <Form dispatch={dispatch} {...state} />
        {/* confirmation poup */}
        <ConfirmPopup
          luckydraw={state.luckydraw}
          isOpen={state.app.isConfirming}
          isProcessing={state.app.isProcessing}
          close={(e) => dispatch(handleEditForm())}
          handleSubmit={() =>
            dispatch(handleLuckyDrawFormSubmit(state.luckydraw))
          }
        />
        {/* thank you poup */}
        <LuckyDrawThankyouPopup
          isOpen={state.order.isSuccess}
          close={(e) => dispatch(resetValues())}
          onClose={(e) => dispatch(resetValues())}
        />
        {/* form error */}
        <ErrorSnack
          open={state.app.isNetworkError}
          setOpen={() => dispatch(resetSubmissionError())}
          message={"Oops something went wrong! Please try again"}
        />
      </Container>
      {/* Footer */}
      <Footer />
      {/* consent */}
      <CookieConsent
        location="bottom"
        buttonText="Accept all cookies"
        cookieName="esso-lpg-cookie-conset"
        style={{ background: "#fff", color: "#333", padding: "5px" }}
        buttonStyle={{
          color: "#fff",
          fontSize: "13px",
          background: "#0C479D",
        }}
        expires={150}
        onAccept={() => {
          ReactGA.initialize(GA_ID);
          ReactGA.pageview("/");
        }}
      >
        By clicking “Accept all cookies”, you agree to the storing of optional
        cookies on your device to enhance site navigation, analyze site usage,
        and assist in our marketing efforts. You can also learn more by clicking{" "}
        <a
          href="https://corporate.exxonmobil.com/Global-legal-pages/privacy-policy"
          target="_blank"
        >
          Privacy Policy
        </a>
      </CookieConsent>
    </Container>
  );
};