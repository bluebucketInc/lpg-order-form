// lib imports
import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
// material
import { ThemeProvider } from "@material-ui/styles";
// utils
import { theme } from "./styles/theme";
// pages
import { SubmissionForm } from "./submission_form";
import { LuckyDrawForm } from "./luckydraw_form";
import { TermsPage } from "./terms-and-conditions";

// App
const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route exact path="/">
            <SubmissionForm />
          </Route>
          <Route path="/2021_luckydraw">
            <LuckyDrawForm />
          </Route>
          <Route path="/terms-and-conditions">
            <TermsPage />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

// export
export default App;
