
// lib imports
import React from 'react';
import ReactDOM from 'react-dom';
import { init } from '@sentry/browser';

// styles
import 'normalize.css';
import "./styles/_app.scss";

// components
import App from './app';

// error tracking
init({dsn: "https://93425d43590243b5ad84c0cfe61240c0@sentry.io/1508659"});

// dom render
ReactDOM.render(<App />, document.getElementById('root'));