
// lib imports
import React from "react";


// styles
import "../styles/_footer.scss";

// footer component
const Footer = () => (
    <div className="footer-wrapper">
        <div className="logos">
            <a className="exxonmobil" href="https://www.exxonmobil.com.sg" target="_blank" rel="noopener noreferrer">ExxonMobil</a>
            <div className="related-companies">
                <a href="http://www.exxon.com/" className="exxon" target="_blank" rel="noopener noreferrer">Exxon</a>
                <a href="http://www.mobil.com/" target="_blank" rel="noopener noreferrer" className="mobil">Mobil</a>
                <a href="http://www.esso.com" target="_blank" rel="noopener noreferrer" className="esso">Esso</a>
            </div>
        </div>
        <div className="bottom-nav-list">
           <div className="links">
               <a href="https://www.exxonmobil.com.sg/en-sg/legal-pages/privacy-policy" target="_blank" rel="noopener noreferrer">Privacy policy</a>
               <a href="https://www.exxonmobil.com.sg/en-sg/legal-pages/terms-and-conditions" target="_blank" rel="noopener noreferrer">Terms &amp; conditions</a>
            </div>
           <div className="copyright">© Copyright 2003-2019 &nbsp; Exxon Mobil Corporation. All Rights Reserved</div>
        </div>
    </div>
)

// module export
export default Footer;