
// lib imports
import React from "react";
import ReactGA from 'react-ga';

// material
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';
import {
	FormControlLabel,
	FormHelperText,
	FormControl,
	FormGroup,
	MenuItem,
	Checkbox,
	Button,
	Grid,
} from "@material-ui/core";

// state
import { handleLuckyDrawFormChange } from '../state/actions';

// utils
import {
	getFormSettings,
	luckydrawItems,
	luckydrawSelectItems,
} from '../utils/form_settings';

// theme
import { generalStyles, formStyles } from "../styles/theme";

// form component
const Form = ({ luckydraw, app, dispatch }) => {

	// get styles
	const classes = formStyles();
	const genClasses = generalStyles();
	// get settings
	const formSettings = getFormSettings(classes);
	// app states
	const { isProcessing, isConfirming, formError } = app;

	// handle order confirmation
	const handleConfirmation = () => {

		// analytics
		ReactGA.event({
			category: 'Order',
			action: 'Confirmed',
		});

		// set form error
		dispatch({
			type: "form-error",
			payload: !luckydraw.terms_consent,
		});

		// if consent is true
		if(luckydraw.terms_consent) {
			// dispatch confirmation
			dispatch({
				type: "order-confirmation",
				payload: true
			});
		}
	}


	// render
	return (
		<ValidatorForm className={genClasses.container} onSubmit={handleConfirmation}>
			<h1 className="form-header">Fill in the form below to enter:</h1>
			<Grid container className={classes.grid} spacing={formSettings.gridSpacing}>
				{	/* text input items */
					luckydrawItems.map((item, index) => 
						<Grid key={item.id} {...formSettings.gridItem}>
							<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
				    			<TextValidator
				    				label={item.label}
				    				value={luckydraw[item.id]}
				    				onChange={e => dispatch(handleLuckyDrawFormChange(e, item.id))}
				    				validators={item.validators}
				    				errorMessages={item.errorMessages}
				    				{...formSettings.textInput} />
							</FormControl>
						</Grid>
					)
				}

				{	/* select input items */
					luckydrawSelectItems.map(item => (
						<Grid key={item.id} {...formSettings.gridItem}>
							<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
								<SelectValidator
									label={item.label}
									value={luckydraw[item.id]}
									onChange={e => dispatch(handleLuckyDrawFormChange(e, item.id))}
									classes={{root: classes.textinput}}
									validators={item.validators}
				    				errorMessages={item.errorMessages}
									{...formSettings.select} >
									{
										item.options.map(option =>
											<MenuItem
												key={option.value}
												value={option.value}>
												{option.label}</MenuItem>)
									}
								</SelectValidator>
							</FormControl>
						</Grid>
					))
				}

				<Grid {...formSettings.gridItem} className={classes.gridItem}>
					<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
						<SelectValidator
							label="Preferred Mode of Contact"
							value={luckydraw.modeOfContact}
							required
							onChange={e => dispatch(handleLuckyDrawFormChange(e, "modeOfContact"))}
							validators={["required"]}
							errorMessages={["This field is required"]}
							classes={{root: classes.textinput}}
							{...formSettings.select} >
							<MenuItem value="email">Email</MenuItem>
							<MenuItem value="phone">Phone</MenuItem>
						</SelectValidator>
					</FormControl>
				</Grid>

				{ /* Checkbox */}
				<Grid className={classes.gridItem} xs={12} item>
					<FormGroup>
						<FormControlLabel
							className={classes.formControlLabel}
							disabled={isProcessing || isConfirming}
							control={
								<Checkbox
									checked={luckydraw.terms_consent}
									color="primary"
									onChange={e => dispatch(handleLuckyDrawFormChange(e, "terms_consent", true))}
									id="terms_consent"
									name="terms_consent"
									value={luckydraw.terms_consent}
								/>}
							label={
								<p style={{margin: 0}}>I agree to the <a href="/#/terms-and-conditions" target="_blank"><strong>terms and conditions</strong></a></p>
							}
						/>
						<div className="text-center">
							{ formError && <FormHelperText error>Please agree to the terms & conditions</FormHelperText>}
						</div>
					</FormGroup>
					<FormGroup>
						<FormControlLabel
							className={classes.formControlLabel}
							disabled={isProcessing || isConfirming}
							control={
								<Checkbox
									checked={luckydraw.mktg_consent}
									color="primary"
									onChange={e => dispatch(handleLuckyDrawFormChange(e, "mktg_consent", true))}
									id="mktg_consent"
									name="mktg_consent"
									value={luckydraw.mktg_consent}
								/>}
							label={
								<p style={{margin: 0}}>I want to be informed about future promotions.</p>
							}
						/>
					</FormGroup>
				</Grid>

				{ /* Confirm button */}
				<Grid className={classes.gridItem} xs={12} item>
					<div className={genClasses.buttonContainer}>
						<Button
							variant="contained"
							disabled={isConfirming}
							type="submit"
							color="primary"
						>
							Submit
						</Button>
					</div>
				</Grid>

			</Grid>
		</ValidatorForm>
	);
};

// export
export default Form;