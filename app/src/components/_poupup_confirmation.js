
// lib imports
import React from "react";
import moment from "moment";

// material
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	Grid,
	CircularProgress
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import { generalStyles } from "../styles/theme";
import { inputItems, selectItems, selectValues } from '../utils/form_settings';

// form component
const ConfirmPopup = ({ values, isOpen, close, handleSubmit, isProcessing }) => {
	// get style
	const classes = generalStyles();
	const popupClasses = popupStyle();
	// render
	return (
		<React.Fragment>
			<Dialog
				disableBackdropClick
				disableEscapeKeyDown
				open={isOpen}
				scroll={"paper"}
				aria-labelledby="popup-confirmation"
			>
				<DialogTitle id="popup-confirmation" className={popupClasses.title}>Please confirm your order:</DialogTitle>
				<DialogContent>
					<Grid container justify="space-between" alignItems="flex-start" spacing={3}>

						{ // input items
							inputItems.map(item =>
								<React.Fragment key={item.id}>
									<Grid className="table-label" item xs={12} sm={6}>{item.label}:</Grid>
									<Grid className="table-value" item xs={12} sm={6}>{values[item.id]}</Grid>
								</React.Fragment>)
						}

						{ // select items
							selectItems.map(item =>
								<React.Fragment key={item.id}>
									<Grid className="table-label" item xs={12} sm={6}>{item.label}:</Grid>
									<Grid className="table-value" item xs={12} sm={6}>
										{getSelectValue(item.id, values)}
									</Grid>
								</React.Fragment>)
						}
						<Grid className="table-label" item xs={12} sm={6}>Preferred Delivery Date:</Grid>
						<Grid className="table-value" item xs={12} sm={6}>{moment(values.delivery_date).format("DD / MM / YY")}</Grid>
						<Grid className="table-label" item xs={12} sm={6}>Preferred Delivery Time:</Grid>
						<Grid className="table-value" item xs={12} sm={6}>{selectValues.timeslot[values.timeslot]}</Grid>
						<Grid className="table-label" item xs={12} sm={6}>Preferred Mode of Contact:</Grid>
						<Grid className="table-value" item xs={12} sm={6}><span style={{textTransform: "capitalize"}}>{values.modeOfContact}</span></Grid>
					</Grid>
				</DialogContent>
				<DialogActions className={popupClasses.footer}>
					<Button
						variant="contained"
						onClick={close}
						disabled={isProcessing}
						color="secondary">Edit</Button>
					<div className={classes.buttonContainer}>
						<Button
							variant="contained"
							onClick={handleSubmit}
							disabled={isProcessing}
							color="primary">Submit</Button>
						{isProcessing && (
							<CircularProgress
								size={24}
								className={classes.buttonProgress}
							/>
						)}
					</div>
				</DialogActions>
			</Dialog>
		</React.Fragment>
	);
};

// select value
const getSelectValue = (id, values) => {
	if (id === "distributor") {
		return (values[id] && (selectValues[id][(values[id])]).name);
	}
	if (id === "existing_customer") {
		return values[id] === "a" ? "Yes" : " No";
	}
	return values[id];
}

// global styles
const popupStyle = makeStyles(theme => ({
	title: {
		padding: "15px 20px 20px"
	},
	footer: {
		marginTop: 10,
	}
}));


// export
export default ConfirmPopup;