// lib imports
import React from "react";

// material
import { Dialog, DialogContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// // components
import { selectValues } from "../utils/form_settings";
// import { ENDPOINTS } from "../utils/";

// Thank you popup
const ThankyouPopup = ({ orderID, isOpen, close, onClose, distributor }) => {
  // get style
  const popupClasses = popupStyle();
  // render
  return (
    <React.Fragment>
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        open={isOpen}
        onClose={onClose}
        scroll={"paper"}
        aria-labelledby="popup-thankyou"
      >
        <DialogContent>
          <h4 className="popup-title">Thank you for ordering.</h4>
          <img
            src={process.env.PUBLIC_URL + "/images/logos/close.png"}
            alt="popup close"
            className="popup-close"
            onClick={close}
          />
          <h6>
            <strong>Order reference number: {orderID}</strong>
          </h6>
          <p>
            A confirmation email for your Esso LPG cylinder order will be sent
            shortly. Your chosen LPG distributor will be in touch with you soon.
          </p>
          <p>
            For enquiries or to change your order, please contact your
            distributor:
          </p>
          <h6>
            <strong>
              {distributor && selectValues.distributor[distributor].name}:{" "}
              {distributor && selectValues.distributor[distributor].phone}
            </strong>
          </h6>
          <div style={{ padding: "0 15px" }}></div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

export const LuckyDrawThankyouPopup = ({ isOpen, close, onClose }) => {
  // get style
  const popupClasses = popupStyle();
  // render
  return (
    <React.Fragment>
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        open={isOpen}
        onClose={onClose}
        scroll={"paper"}
        aria-labelledby="popup-thankyou"
      >
        <DialogContent>
          <div className="thankyou-popup">
            <h4 className="popup-title">Thank you</h4>
            <img
              src={process.env.PUBLIC_URL + "/images/logos/close.png"}
              alt="popup close"
              className="popup-close"
              onClick={close}
            />
            <p>
              for submitting your entry! <strong>Please retain the physical receipt as proof of purchase.</strong> We wish you the best of luck in the
              draw. Winners will be notified via phone and email.
            </p>
            <div style={{ padding: "0 15px" }}></div>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

// global styles
const popupStyle = makeStyles((theme) => ({
  footer: {
    alignItems: "flex-start",
    marginTop: 10,
  },
}));

// export
export default ThankyouPopup;
