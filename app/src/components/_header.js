
// lib imports
import React from "react";

// styles
import "../styles/_header.scss";

// header component
const Header = ({ luckdrawForm = false, terms = false, normal = false }) => (
    <div className="nav-header">
        <div className="logo">
            <img
                src="https://wholesalefuels.exxonmobil.com.sg/~/media/global/wholesale-fuels/esso-wholesale-fuels.png"
                alt="Esso Wholesale Fuels logo"
                title="Esso Wholesale Fuels logo"
            />
        </div>
        <div className="divider" />
        <div className="breadcrumb-panel">
            <div className="breadcrumb-list">
                <a href="https://wholesalefuels.esso.com.sg/en/">Esso</a>
                <a href="https://wholesalefuels.esso.com.sg/en/esso-lpg">Esso LPG</a>
                {luckdrawForm && <a className="last-child">Enter our lucky draw</a>}
                {normal && <a className="last-child">Order Form</a>}
                {terms && <a className="last-child">Terms and conditions</a>}
            </div>
        </div>
    </div>
);

// module export
export default Header;