// lib imports
import React from "react";
import { Helmet } from "react-helmet";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import "../styles/_masthead.scss";

// footer component
const Masthead = () => (
  <div className="masthead">
    <div className="masthead-image-container">
      <img src={"/images/masthead.jpg"} alt="Esso" className="masthead-img" />
      <img
        src={"/images/masthead-mobile.jpg"}
        alt="Esso"
        className="masthead-img mobile"
      />
    </div>
    <div className="content-container text-left">
      <div className="inner-container">
        <h1>Let happiness and comfort surround every home-cooked meal.</h1>
        <p>
          Feed your senses: Aromatics unlocked with a stovetop sizzle and lively
          conversations with friendly faces. At the centre of it all, a cooking
          experience that’s been relied on for years.
        </p>
        <p>
          To cook with Esso LPG is to fuel moments as delightful as this.
          There’s joy all around with reliable, high-quality LPG; now even more
          so with the chance to win up to 3 months’ worth of groceries*.
        </p>
        <p>
          Buy an Esso LPG cylinder from 10 May till 18 July 2021 for a chance to
          enter our lucky draw. With FairPrice vouchers worth $1,500, $1,000, or
          $500 up for grabs, home meals are bound to be all the more satisfying!
        </p>
        <p style={{ fontSize: 14 }}>
          *Based on the average grocery expenditure of a Singaporean household
          (Source: ValueChampion)
        </p>
        <p>
          Disclaimers:
          <br />
          - Orders have to be made before 6pm for next-day delivery.
          <br />
          - Orders and the price of the cylinder will be confirmed by
          distributors via a phone call.
          <br />- Each cylinder purchased will entitle you to one chance at the
          lucky draw.
        </p>
        <a
          href="https://wholesalefuels.esso.com.sg/en-sg/esso-lpg/promotion_faqs"
          target="_blank"
        >
          View our FAQs
        </a>
      </div>
    </div>
  </div>
);

// luckydraw masthead
export const LuckyDrawMasthead = () => (
  <div className="masthead luckydraw">
    {/* title */}
    <Helmet>
      <title>Enter our lucky draw | Esso Wholesale Fuels</title>
      <meta
        name="description"
        content="Buy an Esso LPG cylinder from 10 May till 18 July 2021 to join our lucky draw. Submit your entry here!"
      />
      <meta
        name="keywords"
        content="Esso, Esso LPG, lucky draw, win vouchers, gas cylinder promotion, gas tank promotion, lucky draw entry form"
      />
    </Helmet>
    <div className="masthead-image-container">
      <img
        src={"/images/luckydraw/masthead.jpg"}
        alt="Esso"
        className="masthead-img"
      />
      <img
        src={"/images/luckydraw/masthead-mobile.jpg"}
        alt="Esso"
        className="masthead-img mobile"
      />
    </div>
    <div className="content-container text-left">
      <div className="inner-container">
        <h1>Win up to $1,500 worth of FairPrice vouchers in our lucky draw</h1>
        <Grid container spacing={3}>
          <Grid item xs={12} md={7}>
            <div className="prize-wrapper">
              {/* one */}
              <div className="prize-container">
                <div className="digit-container">
                  <p>
                    <strong>1</strong>
                    <sup>st</sup>
                  </p>
                </div>
                <div className="text-container">
                  <p>$1,500 of FairPrice vouchers</p>
                  <p className="term">(1 winner x 3 draws)</p>
                </div>
              </div>
              {/* two */}
              <div className="prize-container">
                <div className="digit-container">
                  <p>
                    <strong>2</strong>
                    <sup>nd</sup>
                  </p>
                </div>
                <div className="text-container">
                  <p>$1,000 of FairPrice vouchers</p>
                  <p className="term">(1 winner x 3 draws)</p>
                </div>
              </div>
              {/* three */}
              <div className="prize-container">
                <div className="digit-container">
                  <p>
                    <strong>3</strong>
                    <sup>rd</sup>
                  </p>
                </div>
                <div className="text-container">
                  <p>$500 of FairPrice vouchers</p>
                  <p className="term">(1 winner x 3 draws)</p>
                </div>
              </div>
            </div>
            <a
              href="https://wholesalefuels.esso.com.sg/en-sg/esso-lpg/promotion_faqs"
              target="_blank"
            >
              <Button
                variant="contained"
                type="submit"
                color="primary"
                size="small"
              >
                <span className="btn-internal">View our FAQs</span>
              </Button>
            </a>
            <a href="/#/terms-and-conditions">
              <Button
                variant="contained"
                type="submit"
                color="primary"
                size="small"
              >
                <span className="btn-internal">Terms and conditions</span>
              </Button>
            </a>
          </Grid>
          <Grid item xs={12} md={5}>
            <div className="voucher-img-container">
              <img
                src={"/images/luckydraw/voucher.png"}
                className="voucher-image"
                alt="Esso"
              />
            </div>
          </Grid>
        </Grid>
        <h2>
          Stand to win FairPrice vouchers with every Esso LPG cylinder you buy!<br/>
          And with three draws coming up, that’s triple your chances of winning.
        </h2>
        <div className="date-wrapper">
          {/* container */}
          <div className="date-container">
            <div className="ybtn">Lucky Draw 1</div>
            <p>4 June 2021</p>
            <p className="terms">
              Entries submitted from
              <br />
              10 May 2021 to 2 June 2021
              <br />
              (11.59pm)
            </p>
          </div>
          {/* container */}
          <div className="date-container">
            <div className="ybtn">Lucky Draw 2</div>
            <p>2 July 2021</p>
            <p className="terms">
              Entries submitted from
              <br />3 June 2021 to 30 June 2021
              <br />
              (11.59pm)
            </p>
          </div>
          {/* container */}
          <div className="date-container">
            <div className="ybtn">Lucky Draw 3</div>
            <p>23 July 2021</p>
            <p className="terms">
              Entries submitted from
              <br />1 July 2021 to 18 July 2021
              <br />
              (11.59pm)
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
);

// module export
export default Masthead;
