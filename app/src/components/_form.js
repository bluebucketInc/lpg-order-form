
// lib imports
import React from "react";
import moment from "moment";
import ReactGA from 'react-ga';
import DateFnsUtils from '@date-io/date-fns';

// material
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import {
	FormControlLabel,
	FormHelperText,
	FormControl,
	FormGroup,
	MenuItem,
	Checkbox,
	Button,
	Grid,
} from "@material-ui/core";

// state
import { handleFormDateChange, handleFormChange } from '../state/actions';

// utils
import { getminOrderingDate } from '../utils';
import {
	getFormSettings,
	inputItems,
	selectItems,
	todayTimeSlot,
	everydayTimeSlot,
} from '../utils/form_settings';

// theme
import { generalStyles, formStyles } from "../styles/theme";

// form component
const Form = ({ values, app, dispatch }) => {

	// get styles
	const classes = formStyles();
	const genClasses = generalStyles();
	// get settings
	const formSettings = getFormSettings(classes);
	// app states
	const { isProcessing, isConfirming, formError } = app;

	// handle order confirmation
	const handleConfirmation = () => {

		// analytics
		ReactGA.event({
			category: 'Order',
			action: 'Confirmed',
		});

		// set form error
		dispatch({
			type: "form-error",
			payload: !values.terms_consent,
		});

		// if consent is true
		if(values.terms_consent) {
			// dispatch confirmation
			dispatch({
				type: "order-confirmation",
				payload: true
			});
		}
	}

	// display timeslot options
	const timeslotOptions = () => {

		// if date it today
		if (moment().isSame(moment(values.delivery_date), "day")) {
			return todayTimeSlot.map(
				item =>
					item.valid && (
						<MenuItem key={item.value} value={item.value}>
							{item.label}
						</MenuItem>
					)
			);
		} else {
			return everydayTimeSlot.map(item => (
				<MenuItem key={item.value} value={item.value}>
					{item.label}
				</MenuItem>
			));
		}
	};

	// render
	return (
		<ValidatorForm className={genClasses.container} onSubmit={handleConfirmation}>
			<h1 style={{paddingBottom: 20}}>
				Fill in your details to begin your order
			</h1>
			<Grid container className={classes.grid} spacing={formSettings.gridSpacing}>
				{	/* text input items */
					inputItems.map((item, index) => 
						<Grid key={item.id} {...formSettings.gridItem}>
							<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
				    			<TextValidator
				    				label={item.label}
				    				value={values[item.id]}
				    				onChange={e => dispatch(handleFormChange(e, item.id))}
				    				validators={item.validators}
				    				errorMessages={item.errorMessages}
				    				{...formSettings.textInput} />
							</FormControl>
						</Grid>
					)
				}

				{	/* select input items */
					selectItems.map(item => (
						<Grid key={item.id} {...formSettings.gridItem}>
							<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
								<SelectValidator
									label={item.label}
									value={values[item.id]}
									onChange={e => dispatch(handleFormChange(e, item.id))}
									classes={{root: classes.textinput}}
									validators={item.validators}
				    				errorMessages={item.errorMessages}
									{...formSettings.select} >
									{
										item.options.map(option =>
											<MenuItem
												key={option.value}
												value={option.value}>
												{option.label}</MenuItem>)
									}
								</SelectValidator>
							</FormControl>
						</Grid>
					))
				}

				{/* Smile membership */}
				{/* <Grid {...formSettings.gridItem}>
					<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
		    			<TextValidator
		    				label="Smiles Membership (input last 4 digits)"
		    				value={values.smiles_id}
		    				onChange={e => dispatch(handleFormChange(e, "smiles_id"))}
		    				validators={["required"]}
							errorMessages={["This field is required"]}
		    				{...formSettings.textInput} />
					</FormControl>
				</Grid> */}

				{ /* Date picker */}
				<Grid {...formSettings.gridItem} className={classes.gridItem}>
					<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
						<MuiPickersUtilsProvider utils={DateFnsUtils}>
							<KeyboardDatePicker
								{...formSettings.datePicker}
								value={values.delivery_date}
								onChange={date => dispatch(handleFormDateChange(date))}
								minDate={getminOrderingDate()}
								maxDate={moment("2021-07-18")}
								InputProps={{
									disableUnderline: true,
									classes: {
                						root: classes.textinput
            						}
            					}}
            					// shouldDisableDate={date => {
            					// 	// today
								// 	const today = moment(date, 'YYYY/MM/DD');
            					// 	// disable other years
            					// 	if(today.year() === 2021) {
            					// 		// enable only the next two months
            					// 		if(today.month() === 10) {
	            				// 			return false;
	            				// 		} else {
	            				// 			return true;
	            				// 		}
            					// 	} else {
            					// 		return true;
            					// 	}
            					// }}
								inputVariant="filled"
							/>
						</MuiPickersUtilsProvider>
					</FormControl>
				</Grid>

				{ /* Time slot picker */}
				<Grid {...formSettings.gridItem} className={classes.gridItem}>
					<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
						<SelectValidator
							{...formSettings.select}
							label="Preferred Delivery Time"
							value={values.timeslot}
							required
							validators={["required"]}
							errorMessages={["This field is required"]}
							onChange={e => dispatch(handleFormChange(e, 'timeslot'))}
							>
							{timeslotOptions()}
						</SelectValidator>
					</FormControl>
				</Grid>
				<Grid {...formSettings.gridItem} className={classes.gridItem}>
					<FormControl {...formSettings.formControl} disabled={isProcessing || isConfirming}>
						<SelectValidator
							label="Preferred Mode of Contact"
							value={values.modeOfContact}
							required
							onChange={e => dispatch(handleFormChange(e, "modeOfContact"))}
							validators={["required"]}
							errorMessages={["This field is required"]}
							classes={{root: classes.textinput}}
							{...formSettings.select} >
							<MenuItem value="email">Email</MenuItem>
							<MenuItem value="phone">Phone</MenuItem>
						</SelectValidator>
					</FormControl>
				</Grid>

				{ /* Checkbox */}
				<Grid className={classes.gridItem} xs={12} item>
					<FormGroup>
						<FormControlLabel
							className={classes.formControlLabel}
							disabled={isProcessing || isConfirming}
							control={
								<Checkbox
									checked={values.terms_consent}
									color="primary"
									onChange={e => dispatch(handleFormChange(e, "terms_consent", true))}
									id="terms_consent"
									name="terms_consent"
									value={values.terms_consent}
								/>}
							label={
								<p style={{margin: 0}}>I agree to the <a href="/#/terms-and-conditions" target="_blank" rel="noopener noreferrer"><strong>terms and conditions</strong></a></p>
							}
						/>
						<div className="text-center">
							{ formError && <FormHelperText error>Please agree to the terms & conditions</FormHelperText>}
						</div>
					</FormGroup>
					<FormGroup>
						<FormControlLabel
							className={classes.formControlLabel}
							disabled={isProcessing || isConfirming}
							control={
								<Checkbox
									checked={values.mktg_consent}
									color="primary"
									onChange={e => dispatch(handleFormChange(e, "mktg_consent", true))}
									id="mktg_consent"
									name="mktg_consent"
									value={values.mktg_consent}
								/>}
							label={
								<p style={{margin: 0}}>I want to be informed about future promotions.</p>
							}
						/>
					</FormGroup>
				</Grid>

				{ /* Confirm button */}
				<Grid className={classes.gridItem} xs={12} item>
					<div className={genClasses.buttonContainer}>
						<Button
							variant="contained"
							disabled={isConfirming}
							type="submit"
							color="primary"
						>
							Confirm
						</Button>
					</div>
				</Grid>

			</Grid>
		</ValidatorForm>
	);
};

// export
export default Form;