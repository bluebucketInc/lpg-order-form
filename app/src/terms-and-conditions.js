// lib imports
import React, { useState } from "react";
import CookieConsent from "react-cookie-consent";
import ReactGA from "react-ga";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import { Helmet } from "react-helmet";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
// material
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
// utils
import { generalStyles } from "./styles/theme";
// components
import Header from "./components/_header";
import Footer from "./components/_footer";
import { GA_ID } from "./utils";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  summary: {
    background: "#F0F0F0",
    boxShadow: "none",
    border: "0",
  },
  heading: {
    fontSize: theme.typography.pxToRem(16),
    fontFamily: "EM Regular",
  },
}));

// lucky draw form
export const TermsPage = () => {
  // hooks
  const customClasses = useStyles();
  const classes = generalStyles();
  // state
  const [expanded, setExpanded] = useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  // render
  return (
    <Container maxWidth="lg" className="outerWrapper">
      {/* Header */}
      <Header terms />
      {/* Masthead */}
      <div className="masthead luckydraw">
        {/* title */}
        <Helmet>
          <title>Lucky draw terms and conditions | Esso Wholesale Fuels</title>
          <meta
            name="description"
            content="Buy an Esso LPG cylinder from 10 May till 18 July 2021 to join our lucky draw. View terms and conditions here."
          />
          <meta
            name="keywords"
            content="Esso, Esso LPG, lucky draw, win vouchers, gas cylinder promotion, gas tank promotion, lucky draw terms and conditions, terms and conditions"
          />
        </Helmet>
        <div className="masthead-image-container">
          <img
            src={"/images/luckydraw/masthead.jpg"}
            alt="Esso"
            className="masthead-img"
          />
          <img
            src={"/images/luckydraw/masthead-mobile.jpg"}
            alt="Esso"
            className="masthead-img mobile"
          />
        </div>
        <div className="content-container text-left">
          <div className="inner-container">
            <h2>Esso LPG lucky draw terms and conditions</h2>
            <div className="accordion-container">
              {terms?.map((i, n) => (
                <>
                  <Accordion
                    key={n}
                    expanded={expanded === i?.id}
                    onChange={handleChange(i?.id)}
                  >
                    <AccordionSummary
                      className={customClasses?.summary}
                      expandIcon={
                        expanded === i?.id ? (
                          <RemoveCircleOutlineIcon />
                        ) : (
                          <AddCircleOutlineIcon />
                        )
                      }
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography className={customClasses?.heading}>
                        {i?.title}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>{i?.terms}</Typography>
                    </AccordionDetails>
                  </Accordion>
                  <div style={{ margin: "15px 0" }}></div>
                </>
              ))}
            </div>
          </div>
        </div>
      </div>
      {/* form */}
      <Container maxWidth="md" className={classes.wrapper}></Container>
      {/* Footer */}
      <Footer />
      {/* consent */}
      <CookieConsent
        location="bottom"
        buttonText="Accept all cookies"
        cookieName="esso-lpg-cookie-conset"
        style={{ background: "#fff", color: "#333", padding: "5px" }}
        buttonStyle={{
          color: "#fff",
          fontSize: "13px",
          background: "#0C479D",
        }}
        expires={150}
        onAccept={() => {
          ReactGA.initialize(GA_ID);
          ReactGA.pageview("/");
        }}
      >
        By clicking “Accept all cookies”, you agree to the storing of optional
        cookies on your device to enhance site navigation, analyze site usage,
        and assist in our marketing efforts. You can also learn more by clicking{" "}
        <a
          href="https://corporate.exxonmobil.com/Global-legal-pages/privacy-policy"
          target="_blank"
        >
          Privacy Policy
        </a>
      </CookieConsent>
    </Container>
  );
};

const terms = [
  {
    id: "panel1",
    title: "Eligibility",
    terms: (
      <ol>
        <li>
          This Promotion is open to all customers who have purchased an Esso LPG
          cylinder, during the Promotion Period (defined in clauses 2 and 5
          below), either through our Authorised Distributors – Tan Bros Gas
          Supply <strong>or</strong> Tan Soon Huah Gas Supply{" "}
          <strong>or</strong> Mega Gas Enterprise Pte Ltd, <strong>or</strong>{" "}
          via our <a href="/">online order form</a> (“Eligible Participants”).{" "}
        </li>
        <li>
          Eligible Participants will receive a leaflet upon receipt of their
          Esso LPG cylinder featuring a QR code. Upon scanning the QR code,
          Eligible Participants will be taken to the Lucky Draw{" "}
          <a href="/#/2021_luckydraw">submission form</a>, where they will need
          to enter their particulars to be entered into the draw from 10 May to
          18 July 2021 (“Promotion Period”).
        </li>
        <li>
          A physical receipt (provided by the Distributor upon delivery of the
          LPG tank) <strong>must</strong> be retained as proof of purchase.
          Lucky Draw winners will need to present this receipt upon collection
          of their Prize. Winners who fail to produce the physical receipt will
          not be allowed to claim their Prize. Photocopies, photographs of
          receipts and/or other duplications and/or reproductions are{" "}
          <strong>not permitted</strong>.
        </li>
        <li>
          Incomplete, inaccurate or illegible entries will be disqualified.
        </li>
      </ol>
    ),
  },
  {
    id: "panel2",
    title: "Promotion Period",
    terms: (
      <ol start="5">
        <li>
          The Promotion is valid from{" "}
          <strong>10 May 2021 to 18 July 2021</strong> (both dates inclusive).
        </li>
      </ol>
    ),
  },
  {
    id: "panel3",
    title: "Promotion Mechanics",
    terms: (
      <ol start="6">
        <li>
          During this Promotion Period, three (3) Lucky Draws will be undertaken
          on the following dates:
          <ol type="a">
            <li>Friday 4 June 2021</li>
            <li>Friday 2 July 2021</li>
            <li>Friday 23 July 2021</li>
          </ol>
        </li>
        <li>
          Three Prizes <strong>per draw</strong> will be on offer for Eligible
          Participants, thus there will be a total of nine (9) winners for each
          draw.
          <br />
          <ol type="a">
            <li>
              <strong>Lucky Draw 1 – Friday 4 June 2021</strong>
              <ol type="i">
                <li>
                  One Thousand Five Hundred dollars’ worth of FairPrice vouchers
                  (SGD1,500) will be given for first
                </li>
                Prize
                <li>
                  One Thousand dollars’ worth of FairPrice vouchers (SGD1,000)
                  will be given for second Prize
                </li>
                <li>
                  Five hundred dollars’ worth of FairPrice vouchers (SGD500)
                  will be given for third Prize
                </li>
              </ol>
            </li>
            <li>
              <strong>Lucky Draw 2 – Friday 2 July 2021</strong>
              <ol type="i">
                <li>
                  One Thousand Five Hundred dollars’ worth of FairPrice vouchers
                  (SGD1,500) will be given for first Prize
                </li>
                <li>
                  One Thousand dollars’ worth of FairPrice vouchers (SGD1,000)
                  will be given for second Prize
                </li>
                <li>
                  Five hundred dollars’ worth of FairPrice vouchers (SGD500)
                  will be given for third Prize
                </li>
              </ol>
            </li>
            <li>
              <strong>Lucky Draw 3 – Friday 23 July 2021</strong>
              <ol type="i">
                <li>
                  One Thousand Five Hundred dollars’ worth of FairPrice vouchers
                  (SGD1,500) will be given for first Prize
                </li>
                <li>
                  One Thousand dollars’ worth of FairPrice vouchers (SGD1,000)
                  will be given for second Prize
                </li>
                <li>
                  Five hundred dollars’ worth of FairPrice vouchers (SGD500)
                  will be given for third Prize
                </li>
              </ol>
            </li>
          </ol>
        </li>
        <li>
          Eligible entries from Monday 10 May 2021 to Wednesday 2 June 2021
          (11.59pm) will qualify for{" "}
          <span style={{ whiteSpace: "nowrap" }}>Lucky Draw 1</span>.
        </li>
        <li>
          Eligible entries from Thursday 3 June 2021 to Wednesday 30 June 2021
          (11.59pm) will qualify for{" "}
          <span style={{ whiteSpace: "nowrap" }}>Lucky Draw 2</span>.
        </li>
        <li>
          Eligible entries from Thursday 1 July 2021 to Sunday 18 July 2021
          (11.59pm) will qualify for{" "}
          <span style={{ whiteSpace: "nowrap" }}>Lucky Draw 3</span>.
        </li>
        <li>
          Unless EMAPPL notifies otherwise, the Lucky Draw will be conducted in
          the presence of an external auditor on the above Lucky Draw Dates
          referred to in clause 7, at 2pm (or on such other dates as EMAPPL may
          determine at its sole and absolute discretion), via remote video call,
          or at such other venue as may be determined by EMAPPL.
        </li>
        <li>
          Lucky Draw winners will be contacted by EMAPPL and/or its appointed
          contractors, via phone and email, between the hours of{" "}
          <span style={{ whiteSpace: "nowrap" }}>9.00am – 9.00pm</span>,
          provided that a valid email address and telephone number is clearly
          given and captured. <strong>Three</strong> contact attempts – during
          the stated working hours – with Lucky Draw winners will be made via
          phone and one follow-up email sent. In the event that EMAPPL and/or
          its appointed contractors is/are unable to contact any winner for any
          reason through this first phone call, EMAPPL and/or its appointed
          contractors will make a further two (2) attempts to contact the winner
          between the hours of{" "}
          <span style={{ whiteSpace: "nowrap" }}>9.00am – 9.00pm</span> of the
          same day that the first call was made. If all such attempts are
          unsuccessful, EMAPPL reserves the right and has the absolute
          discretion to award the Prize to the next reserve winner drawn by
          EMAPPL. Refer to clause 13.
        </li>
        <li>
          Lucky Draw winners will have to claim their Prize within two (2) weeks
          of the winner being successfully notified. If the winner of the Prize
          does not acknowledge the Prize he/she has won by the end of such
          stipulated period, EMAPPL reserves the right and has the absolute
          discretion to award the Prize to the next reserve winner drawn by
          EMAPPL.
        </li>
        <li>
          Where Lucky Draw winners are contactable and confirm their willingness
          to collect their Prize, EMAPPL will request them to verify proof of
          purchase. Upon such verification of this receipt copy, the EMAPPL will
          arrange a date and time with the winner for Prize collection at 1
          HarbourFront Place #06-00, HarbourFront Tower One, Singapore 098633.
        </li>
        <li>
          At the point of collection, Lucky Draw winners will need to physically
          sign an Acknowledgement Form confirming that they have received their
          Prize. If the Lucky Draw winner does not acknowledge the Prize, EMAPPL
          reserves the right and has the absolute discretion to award the Prize
          to the next reserve winner drawn. Note for each draw, a total of 10
          reserve winners will be selected.
        </li>
        <li>
          By accepting the Prize, the winner hereby agrees to be photographed
          and consents to EMAPPL’s use of his/ her name and image for publicity
          purposes in connection with the Promotion, without any further
          compensation and/or further notification.
        </li>
        <li>
          All Prizes cannot be exchanged for cash, other denominations or
          goods/services, and are subject to prevailing terms and conditions of
          use.
        </li>
        <li>
          Any Prizes not collected by Saturday 6 August 2021, will be forfeited
          and donated to a charity of EMAPPL’s choice.
        </li>
      </ol>
    ),
  },
  {
    id: "panel4",
    title: "Participation and Personal Data",
    terms: (
      <ol start="19">
        <li>
          By participating in this Promotion, Eligible Participants are subject
          to these Terms and Conditions. The Eligible Participant shall consent
          and/or is deemed to have consented, to the collection, use, and
          disclosure of his/her Personal Data as set out in the clauses below,
          or as may be set out in any subsequent revised versions of the same.
        </li>

        <li>
          EMAPPL may disclose the Eligible Participants’ Personal Data in
          accordance with the provisions of the PDPA (including all subsidiary
          legislation made thereunder), or where otherwise required by law.
        </li>

        <li>
          The Eligible Participants also agree that EMAPPL may use the Personal
          Data to provide the Eligible Participants with marketing materials
          relating to LPG-related promotions or other LPG related programmes
          developed by EMAPPL from time to time (collectively, "Marketing
          Materials"). EMAPPL may transfer the Personal Data to the Specified
          Third Parties for the purpose of providing Marketing Materials to the
          Eligible Participant. EMAPPL will not disclose the Personal Data to
          other third parties for marketing by such third parties without the
          Eligible Participant's consent.
        </li>

        <li>
          The Eligible Participant may require EMAPPL or any of the persons
          stated in clause 21 not to use the Personal Data for direct marketing
          purposes, and in such an event, the said Eligible Participant will no
          longer be eligible to participate in the Lucky Draw. The Eligible
          Participant may also choose to opt-out of receiving the Marketing
          Materials after the Promotion Period by calling the Esso LPG Customer
          Service hotline at 1800-266-2828 during its operating hours or through
          other modes notified to the Eligible Participant. The Eligible
          Participant will cease to receive the Marketing Materials within
          thirty (30) days after receipt of his/her request to opt out.
        </li>
      </ol>
    ),
  },
  {
    id: "panel5",
    title: "Other Terms and Conditions",
    terms: (
      <ol start="23">
        <li>
          EMAPPL expressly disclaims and shall not be responsible or liable for
          any claim, loss or damage whatsoever or howsoever arising out of or
          whether directly or indirectly in connection with the Promotion or the
          voucher/Prize(s) awarded during the Promotion.
        </li>
        <li>
          For any further enquiries, please contact Esso Smiles LPG Customer
          Service hotline at 1800-266-2828, Monday to Friday (public holidays
          excluded), between 8:00am to 5:00pm.
        </li>
        <li>
          Draw chances accumulated are non-transferable and cannot be combined
          with draw chances accumulated by any other Eligible Participant.
        </li>
        <li>
          There is no limit to the number of entries, but each entry must be
          accompanied with a unique receipt number. Each Eligible Participant is
          eligible to win only one Prize and must produce a physical receipt
          when requested as proof of purchase. There should be no attempt made
          by any Participant to submit a draw submission using a receipt from
          another household.
        </li>
        <li>
          The results of all three (3) Lucky Draws will be published on the{" "}
          <a
            href="https://wholesalefuels.esso.com.sg/en-sg/esso-lpg/promotion_faqs"
            target="_blank"
          >
            Promotion FAQs page
          </a>{" "}
          within 7 working days from each Draw Date.
        </li>
        <li>
          EMAPPL shall not be responsible or liable for any defect or
          malfunction in the Prize and/or for any loss, injury, damage or harm
          suffered or incurred or in connection with the receipt, use or
          enjoyment of the Prize by any person.
        </li>
        <li>
          The terms and conditions for this Promotion may be revised from time
          to time at EMAPPL’s sole discretion without prior notice.
        </li>
        <li>
          EMAPPL’s decision on all matters relating to this Promotion shall be
          final and binding. EMAPPL reserves the right not to enter into any
          correspondence and/or communication with any person or party in
          relation to such decisions.
        </li>
      </ol>
    ),
  },
];
