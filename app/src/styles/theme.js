// m.design
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";

// global styles
export const generalStyles = makeStyles(theme => ({
	wrapper: {
		marginTop: 40,
		marginBottom: 40,
		position: "relative",
	},
	container: {
		marginTop: 40,
		display: "flex",
		flexWrap: "wrap",
	},
	buttonContainer: {
		position: "relative",
		textAlign: "center",
	},
	buttonProgress: {
		color: '#0e469b',
		position: "absolute",
		top: "50%",
		left: "50%",
		marginTop: -12,
		marginLeft: -12
	},
}));

// custom styles
export const formStyles = makeStyles(theme => ({
	grid: {
		margin: theme.spacing(-2.5),
	},
	icon: {
		fontSize: 30,
		opacity: 1,
	},
	formControl: {
		width: "100%",
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
		marginBottom: theme.spacing(2),
	},
	formControlLabel: {
		width: "100%",
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
	},
	textinput: {
		borderRadius: 0,
		marginTop: 0,
	},
	gridItem: {
		paddingTop: "5px !important",
	}
}));

// theme
export const theme = createMuiTheme({
	typography: {
		fontSize: 16,
		fontFamily: 'EM Regular',

	},
	palette: {
		primary: {
			main: '#0e469b',
			contrastText: "#ffffff",
		},
		secondary: {
			main: '#00a3e0',
			contrastText: "#ffffff",
		}
	},
	overrides: {
		MuiCssBaseline: {
			'@global': {
				'@font-face': 'EM Regular',
			},
		},
		MuiButton: {
			root: {
				margin: 10,
				fontFamily: 'EM SemiBold',
				padding: "5px 55px",
		    	fontSize: 18,
				borderRadius: 0,
				textTransform: "none",
			}
		},
		MuiFormHelperText: {
			root: {
				fontSize: 16,
			}
		}
	},
});
