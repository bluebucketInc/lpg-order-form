# Form

## Installation

Install dependencies
> npm install

Run application locally (make sure server is running as well)
> npm run start

Create build folder locally
> npm run build

For production, copy build content to server using FTP.


## Production Config

Update below config variables to reflect production environment

> GA_ID

## Monitor Application Health

Application is monitored using [Sentry](https://sentry.io/organizations/blue-bucket/issues/?project=1508659). Access details are in google drive - *Creds*.

## Important scripts & files

Script controlling minimum delivery date`getminOrderingDate`
> src/utils/index.js

All form settings including validation, form styles, form labels, and form values. Select box value to label converter function is `selectValues` change copy here to reflect popup confirmation values. Delivery time validation is handled by `todayTimeSlot`, `everydayTimeSlot`.
>src/utils/form_settings.js

API calls and all state actions. Two API calls are needed with server `getAccessToken` & `handleFormSubmit`.
> src/state/actions.js