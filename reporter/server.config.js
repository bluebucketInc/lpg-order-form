// PM2 server config
module.exports = {
    apps: [
        {
            name: "reporting-app",
            script: "npm",
            cwd: "/var/www/reporter",
            args: "run production",
        }
    ]
};
