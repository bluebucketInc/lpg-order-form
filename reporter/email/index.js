// import plugins
import mailgun from "mailgun-js";
import moment from "moment";
// import { getDist } from "../utils";
const path = require("path");

// settings
import {
	MG_API_KEY,
	MG_DOMAIN,
	MG_TEMPLATE_REPORT,
	MG_FROM_ADDRESS,
	PDF_ENDPOINT,
	DOMAIN,
	IS_DEV,
} from "../utils/config";

// Mailgin instance
const mGun = mailgun({ apiKey: MG_API_KEY, domain: MG_DOMAIN });

// send order confirmation
export const sendReportEmail = (file) =>
	new Promise((resolve, reject) => {
		// trigger mGun
		mGun.messages().send(
			{
				from: MG_FROM_ADDRESS,
				// to: "bluebucket.dev@gmail.com",
				to: IS_DEV
					? "bluebucket.dev@gmail.com"
					: `leah.layhiong.ng@exxonmobil.com,ivan.bok@exxonmobil.com,jestyn.lim@exxonmobil.com`,
				subject: `Esso LPG Report for ${moment(Date.now()).format(
					"DD/MM/YY"
				)}`,
				template: MG_TEMPLATE_REPORT,
				"h:X-Mailgun-Variables": JSON.stringify({
					order_id: "test",
				}),
				attachment: path.join(__dirname, "..", "static", file),
			},
			(error, body) => (error ? reject(error) : resolve(body))
		);
	});

// send report receipt
export const sendReportReceipt = (status, err) =>
	new Promise((resolve, reject) => {
		console.log(err);
		// trigger mGun
		mGun.messages().send(
			{
				from: MG_FROM_ADDRESS,
				to: "bluebucket.dev@gmail.com",
				subject: `Esso LPG Report for ${moment(Date.now()).format(
					"DD/MM/YY"
				)} was ${status}`,
				template: MG_TEMPLATE_REPORT,
				"h:X-Mailgun-Variables": JSON.stringify({
					order_id: "test",
				})
			},
			(error, body) => (error ? reject(error) : resolve(body))
		);
	});