// import plugins
import moment from "moment";
import mongoose from "mongoose";

// settings
import { DB_URL, DB_USER, DB_PASSWORD } from "../utils/config";

// schema
import lpg_form from "../db/db.schema";

// connect to db
mongoose
	.connect(DB_URL, {
		useNewUrlParser: true,
		user: DB_USER,
		pass: DB_PASSWORD,
	})
	.then((db) => console.log("connection successful"))
	.catch((e) => console.log("connection error", e));

// get report
export const getReport = () =>
	new Promise((resolve, reject) => {
		// report date
		const date = moment().subtract(1, "days").set({hour: 17, minute: 59}).toDate();
		// get documents
		lpg_form.find(
			{
				created_at: { $gte: date },
			},
			function (err, docs) {
				// error
				if(err) {
					reject(err);
					return;
				}
				// resolve
				resolve(docs);
			}
		);
	});