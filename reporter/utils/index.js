// imports
import randomize from "randomatic";
import moment from "moment";
const { Parser } = require("json2csv");
const fs = require("fs");

// generate CSV
export const generateCSV = (data) => {
	// fields
	const fields = [
		{ label: "Block/Building Name", value: "blk" },
		{ label: "Preferred Delivery Date", value: "delivery_date" },
		{ label: "Distributor", value: "distributor" },
		{ label: "Email", value: "email" },
		{ label: "LPG customer (a = Yes, b = No)", value: "existing_customer" },
		{ label: "First Name", value: "first_name" },
		{ label: "Last Name", value: "last_name" },
		{ label: "Marketing consent", value: "mktg_consent" },
		{ label: "Order date", value: "order_date" },
		{ label: "Order ID", value: "order_id" },
		{ label: "Contact Number", value: "phone" },
		{ label: "Postal Code", value: "postcode" },
		{ label: "Quantity", value: "quantity" },
		{ label: "Smiles Membership", value: "smiles_id" },
		{ label: "Street Name", value: "street" },
		{ label: "Terms & conditions", value: "terms_consent" },
		{ label: "Preferred Delivery Time", value: "timeslot" },
		{ label: "Unit Number", value: "unit" },
	];
	// parser
	const json2csvParser = new Parser({ fields });
	// return data
	return json2csvParser.parse(data);
};


// write to file
export const writetoCSV = data =>
	new Promise((resolve, reject) => {
		// convert to csv
		const csv = generateCSV(data);
		const fileName = `${moment(Date.now()).format("DD-MM-YY")}_${randomize('Aa0', 3)}_report.csv`;
		//write to file
		fs.writeFile(`./static/${fileName}`, csv, "utf8", function (err) {
			if (err) {
				reject("Some error occured - file either not saved or corrupted file saved.");
			} else {
				resolve(fileName)
			}
		});
	});


