// import plugins
import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
// import moment from 'moment'
const CronJob = require("cron").CronJob;

// modules
import { writetoCSV } from "./utils/";
import { API_PORT } from "./utils/config";
import { getReport } from "./db";
import { sendReportEmail, sendReportReceipt } from "./email";

// init server
const server = express();

// add middleware
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// security
server.use(helmet());

// broadcast
server.listen(API_PORT + 1, () =>
	console.log(`server running on port ${API_PORT + 1}`)
);

// cronjob
const job = new CronJob(
	// everyday at 5.55PM
	"01 18 * * *",
	function () {
		// get report if it's time
		getReport()
			// write to CSV
			.then((docs) => writetoCSV(docs))
			// send email
			.then((file) => sendReportEmail(file))
			// sucess / fail notification
			.then(() => sendReportReceipt("sucess"))
			// sucess / fail notification
			.catch((err) => sendReportReceipt("fail", err));
	},
	null,
	false,
	"Asia/Singapore"
);
// init
job.start();